//
//  partypicTests.swift
//  partypicTests
//
//  Created by João Henrique Carneiro on 7/15/16.
//  Copyright (c) 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import XCTest

class partypicTests: XCTestCase {
    
    // MARK: Group class tests.
    func testgGroupInitialization() {
        // Success test
        let good_group = Group(title: "group name", hash: "AAA", members: ["me"], avatar: nil)
        XCTAssertNotNil(good_group)
        
        // Failure tests
        let no_hash = Group(title: "group name", hash: "", members: ["me"], avatar: nil)
        XCTAssertNil(no_hash, "No hash is invalid.")
        
        let no_members = Group(title: "group name", hash: "AAA", members: [], avatar: nil)
        XCTAssertNil(no_members, "Zero members is invalid")
    }
}
