//
//  Orientation.swift
//  partypic
//
//  Created by João Henrique Carneiro on 3/1/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

class Orientation {
    static let UP: Int = 0
    static let DOWN: Int = 180
    static let RIGHT: Int = 90
    static let LEFT: Int = -90
}
