//
//  ViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 1/26/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import UIKit

class _KeyboardViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
