//
//  PressAndSwipeGestureRecognizer.swift
//  partypic
//
//  Created by João Henrique Carneiro on 2/22/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

class PressAndSwipeGestureRecognizer: UIGestureRecognizer {

    var minimumPressDuration: CFTimeInterval = 0.5
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
        if touches.count != 2 {
            state = .failed
        }
        state = .began
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        state = .ended
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesMoved(touches, with: event)
        if state == .failed {
            return
        }
        
        
    }
    
}
