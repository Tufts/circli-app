//
//  GroupMainBackground.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/13/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit

enum ViewType : Int {
    case Feed = 0
    case Grid = 1
}

class GroupMainBackground: SWRevealViewController {

    // MARK: Properties
    var auth: Authentication?
    var group: Group?
    var viewtype = ViewType.Feed
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "sw_front" {
            let tabController = segue.destination as! UITabBarController
            
            // First tab is the Photo Table
            var navController = tabController.viewControllers?[ViewType.Feed.rawValue] as! UINavigationController
            let photoTableViewController = navController.topViewController as! PhotoTableViewController
            photoTableViewController.auth = self.auth
            photoTableViewController.group = self.group
            
            // Second tab is the Photo Collection
            navController = tabController.viewControllers?[ViewType.Grid.rawValue] as! UINavigationController
            let photoCollectionViewController = navController.topViewController as! PhotoCollectionViewController
            photoCollectionViewController.auth = self.auth
            photoCollectionViewController.group = self.group
            
            tabController.selectedIndex = viewtype.rawValue
        }
        else if segue.identifier == "sw_right" {
            let menuController = segue.destination as! MenuPhotoTableViewController
            menuController.auth = self.auth
            menuController.group = self.group
        }
    }

}
