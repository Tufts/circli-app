 //
//  PhotoTableViewCell.swift
//  partypic
//
//  Created by João Henrique Carneiro on 8/27/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

/**
 **PhotoTableViewCell** - Displays an image along with information pertaining to that image. Must be configured using .configure(()
*/
class PhotoTableViewCell: UITableViewCell {

    // MARK: Properties
    var auth: Authentication!
    var group: Group!
    var photo: Photo!
    var parent: PhotoTableViewController?
    
    
    // MARK: Storyboard elements
    @IBOutlet weak var img_photo: UIImageView!
    @IBOutlet weak var img_avatar: UIImageView!
    
    @IBOutlet weak var lb_photoOwner: UILabel!
    @IBOutlet weak var lb_caption: UILabel!
    @IBOutlet weak var lb_likes: UILabel?
    @IBOutlet weak var lb_comments: UILabel?
    @IBOutlet weak var lb_date: UILabel!
    
    @IBOutlet weak var bt_like: UIButton?
    @IBOutlet weak var bt_comment: UIButton?
    
    @IBOutlet weak var c_photoWidth: NSLayoutConstraint!
    @IBOutlet weak var c_photoHeight: NSLayoutConstraint!
    
    
    var c_height: CGFloat?
    
    @IBOutlet weak var caption_widthConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // First save the caption height, in case we need it.
        c_height = lb_caption.frame.height
    }
    
    /**
     `Configure` will set the cell with the appropriate data, using the properties set under **MARK:** **Properties**.
    */
    func configure() {
        let height = img_avatar.frame.height
        let width = img_avatar.frame.width
        img_avatar.layer.cornerRadius = max(width,height)/2
        img_avatar.layer.masksToBounds = true;
        
        
        lb_photoOwner.text = photo.owner.name
        
        if photo.caption.isEmpty {
            lb_caption.isHidden = true
            //caption_widthConstraint.constant = 0
        } else {
            lb_caption.text = photo.caption
            lb_caption.isHidden = false
            caption_widthConstraint.constant = c_height!
            
        }
        
        if photo.reactions.count > 0 {
            lb_likes?.text = "\(photo.reactions.count) like(s)"
            lb_likes?.isHidden = false
        } else {
            lb_likes?.isHidden = true
        }
        
        if photo.messages.count > 0 {
            lb_comments?.text = "\(photo.messages.count) comment(s) on this image"
            lb_comments?.isHidden = false
        }
        else {
            lb_comments?.isHidden = true
        }
        
        img_photo.image = nil
        photo.queueImageView(img_photo, completion: addGesturesToImage)
        
        let avatar = photo.owner.avatar
        if avatar != nil {
            avatar!.queueImageView(img_avatar, completion: {})
        }
    }
    
    func adjustImageDimensions() {
    /*
        print("Constraint Height: \(c_photoHeight!.constant)")
        print("Constraint Width: \(c_photoWidth!.constant)")
        print("Row Height: \(self.bounds.height)")
        
        let photo = self.img_photo.image
        let aspectRatio = (photo?.size.height)! / (photo?.size.width)!
        print("Image Height: \(photo!.size.height)")
        print("Image Width: \(photo!.size.width)")
        
        self.c_photoHeight.constant = UIScreen.main.bounds.size.width * aspectRatio
        self.c_photoWidth.constant = UIScreen.main.bounds.size.width
        self.needsUpdateConstraints()
        self.updateConstraints()
        
        print("Constraint Height (adjusted): \(c_photoHeight!.constant)")
        print("Constraint Width (adjusted): \(c_photoWidth!.constant)")
        print("Row Height: \(self.bounds.height)\n")
    */
    }
 
    /**
     Introduces a `UILongPressGesture` to the current cell.
     */
    func addGesturesToImage() {
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(PhotoTableViewCell.longPressed(sender:)))
        self.contentView.addGestureRecognizer(longPressRecognizer)
        
        adjustImageDimensions()
    }
    
    /**
     Introduces behaviour response to `UILongPressGesture`, that shows an Alert
     asking user to confirm the download of said image or not.
     
     - Parameter sender: An `UILongPressGestureRecognizer` already configured to the correct object.
     */
    func longPressed(sender: UILongPressGestureRecognizer) {
        let alertController = UIAlertController(title: "Save image to library",
                                                message: "",
                                                preferredStyle: UIAlertControllerStyle.alert
        )
        
        let saveAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default,
            handler: { alert -> Void in
                let album = CircliAlbum()
                let image = self.img_photo.image!
                
                // Create album if necessary.
                album.createAlbum()
                
                // And save.
                album.saveImage(image: image)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        parent?.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func likePhoto(_ sender: UIButton) {

        Alamofire.request(Router.likePhoto(self.auth!, group: self.group!, photo: self.photo!)!).responseJSON { response in
            let status = response.response?.statusCode
            if status == 200 {
                let json = JSON(response.result.value!)
                if json["Reaction"].exists() {
                    // Was liked.
                    self.lb_likes?.text = "\(self.photo!.reactions.count + 1) like(s)"
                    self.lb_likes?.isHidden = false
                    self.bt_like?.setTitle("Liked", for: UIControlState())
        
                } else {
                    if self.photo!.reactions.count - 1 <  1 {
                        self.lb_likes?.isHidden = true
                    } else {
                        self.lb_likes?.text = "\(self.photo!.reactions.count - 1) like(s)"
                        self.lb_likes?.isHidden = false
                    }
                    self.bt_like?.setTitle("Like this", for: UIControlState())
                }
            }
        }
    }

}
