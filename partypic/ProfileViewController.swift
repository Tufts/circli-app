//
//  ProfileViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 3/5/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var bt_menu: UIBarButtonItem!
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var bt_upload: UIButton!
    @IBOutlet weak var vw_upload: UIView!
    @IBOutlet weak var ind_upload: UIActivityIndicatorView!
    
    var auth: Authentication?
    var admin: User?
    var mode: Int?
    var imagePicker: UIImagePickerController!
    var selected: Bool = false
    var imageOrientation: UIImageOrientation?
    
    var loading: Bool = false {
        didSet {
            vw_upload.isHidden = !loading
            if loading {
                ind_upload.startAnimating()
            } else {
                ind_upload.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading = false

        // Do any additional setup after loading the view.
        if self.revealViewController() != nil {
            bt_menu.target = self.revealViewController()
            bt_menu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        if auth?.refers_to_user?.avatar != nil {
            loading = true
            auth?.refers_to_user?.avatar!.queueImageView(img_profile, completion: {
                self.loading = false
            })
        }
        
        lb_name.text = auth?.refers_to_user!.name
        let height = img_profile.frame.height
        let width = img_profile.frame.width
        img_profile.layer.cornerRadius = max(width,height)/1.65
        img_profile.layer.masksToBounds = true;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func uploadImage(sender: UIButton) {
        selectImage(Upload.LIBRARY)
    }
    func selectImage(_ mode: Int) {
        
        // Probably better to do this camera availability test in the previous view.
        if mode == Upload.CAPTURE {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                takePhotoWithCamera()
            } else {
                let alert: UIAlertView = UIAlertView(title: "Error", message: "Device has no camera", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        } else if mode == Upload.LIBRARY {
            selectImageFromLibrary()
        }
    }
    
    func takePhotoWithCamera() {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func selectImageFromLibrary() {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            img_profile.image = image
            self.imageOrientation = image.imageOrientation
            uploadAvatar(image: image)
            
        } else {
            print("Something wrong")
        }
    }
    
    func uploadAvatar(image: UIImage) {
        let request = Router.uploadAvatar(auth!, image: image, compression: 0.2)
        loading = true
        if request != nil { // Image too big.
            Alamofire.request(request!)
                .responseJSON { response in
                    let status = response.response?.statusCode
                    if status == 200 {
                        // Avatar was succesfully uploaded to server!
                        self.loading = false
                    } else {
                        // Could not send the image!
                        self.loading = false
                        self.warnAndSend(image)
                    }
            }
        }
    }
    
    func warnAndSend(_ image: UIImage) {
        let alertController = UIAlertController(title: "Could not upload to Circli",
                                                message: "Try again?",
                                                preferredStyle: UIAlertControllerStyle.alert
        )
        
        let sureAction = UIAlertAction(title: "Sure", style: UIAlertActionStyle.default, handler: { alert -> Void in
            self.uploadAvatar(image: image)
        })
        
        let forgetAction = UIAlertAction(title: "Never mind", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addAction(sureAction)
        alertController.addAction(forgetAction)
        self.present(alertController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
