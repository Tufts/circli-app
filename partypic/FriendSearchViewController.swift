//
//  FriendSearchViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/26/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FriendSearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: Properties
    var auth: Authentication?
    var users = [User]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var in_query: UITextField!
    @IBOutlet weak var bt_menu: UIBarButtonItem!
    @IBOutlet weak var bt_type: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if self.revealViewController() != nil {
            bt_menu.target = self.revealViewController()
            bt_menu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func searchUsers(_ sender: AnyObject) {
        self.users = [User]()
        self.tableView.reloadData()
        let query = in_query.text!
        let type = 2
        Alamofire.request(Router.findUsers(self.auth!, query: query, type: type)!).responseJSON { response in
            let data = JSON(response.result.value!)
            for (_, entry):(String, JSON) in data["Profiles"] {
                let user = User(json: entry["Profile"])
                self.users.append(user!)
            }
            self.tableView.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "FriendSearchTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FriendSearchTableViewCell
        let user = users[indexPath.row]
        
        cell.auth = self.auth
        cell.lb_username.text = user.name
        
        let avatar = user.avatar
        if avatar != nil {
            avatar!.queueImageView(cell.img_avatar, completion: {})
        }
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
