//
//  Group.swift
//  partypic
//
//  Created by João Henrique Carneiro on 7/22/16.
//  Copyright (c) 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit

class Group {
    
    // MARK: Properties
    var title: String?
    var hash: String
    var members: [String]
    var avatar: Photo?
    
    // MARK: Initialization
    init?(_ title: String?, hash: String, members: [String], avatar: Photo?) {
        self.title = title
        self.hash = hash
        self.members = members
        self.avatar = avatar
        
        if hash.isEmpty || members.count < 1 {
            return nil
        }
    }

}
