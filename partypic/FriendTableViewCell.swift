//
//  FriendTableViewCell.swift
//  partypic
//
//  Created by João Henrique Carneiro on 8/30/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FriendTableViewCell: UITableViewCell {

    // MARK: Properties
    var auth: Authentication?
    var group: Group?
    var friend: User?

    @IBOutlet weak var img_avatar: UIImageView!
    @IBOutlet weak var lb_username: UILabel!
    @IBOutlet weak var lb_realLife: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let height = img_avatar.frame.height
        img_avatar.layer.cornerRadius = height/2;
        img_avatar.layer.masksToBounds = true;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func sendInvite(_ sender: UIButton) {
        Alamofire.request(Router.inviteToGroup(self.auth!, group: self.group!, friend: self.friend!)!).responseJSON { response in
            let status = response.response?.statusCode
            if status == 200 {
                // Succesfully invited friend.
                sender.isEnabled = false
            }
        }
    }

}
