//
//  LoginViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 8/26/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct loginKeys {
    static let user = "username"
    static let pass = "password"
}

class LoginViewController: UIViewController {

    // MARK: Properties
    var handler: FormHandler?
    var device: Device?
    var auth: Authentication?
    var admin: User?
    var activeInput: UITextField?
    
    // MARK: Outlet collections
    @IBOutlet var in_login: [UITextField]!
    
    // MARK: Outlets
    @IBOutlet weak var in_username: UITextField!
    @IBOutlet weak var in_password: UITextField!
    @IBOutlet weak var bt_login: UIButton!
    @IBOutlet weak var lb_error: UILabel!
    @IBOutlet weak var ind_load: UIActivityIndicatorView!
    @IBOutlet weak var confirmScreen: UIView!
    @IBOutlet weak var in_validationCode: UITextField!
    @IBOutlet weak var bt_validate: UIButton!
    @IBOutlet weak var ind_confirm: UIActivityIndicatorView!
    
    // MARK: Default Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        
        
        let defaults = UserDefaults.standard
        
        var bool_user = false;
        var bool_pass = false;
        let username = defaults.string(forKey: loginKeys.user)
        if username != nil {
            in_username.text = username!
            bool_user = true
        }
        
        let password = defaults.string(forKey: loginKeys.pass)
        if password != nil {
            in_password.text = password!
            bool_pass = true
        }
        
        
        if bool_user && bool_pass {
            attemptLogin(bt_login)
        }
        
        handler = FormHandler(inputs: in_login, completion: autoLogin)
        

        // Do any additional setup after loading the view.
        self.confirmScreen.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        bt_login.isEnabled = false
        lb_error.isHidden = true
        in_username.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        in_password.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Actions
    func textFieldDidChange(_ textfield: UITextField) {
        if in_username.text != "" && in_password.text != "" {
            bt_login.isEnabled = true
            lb_error.isHidden = true
        } else {
            bt_login.isEnabled = false
        }
    }
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        handler?.handle(textfield: textfield)
        return true
    }
    
    func autoLogin() {
        attemptLogin(bt_login)
    }
    
    // MARK: Network calls.
    @IBAction func attemptLogin(_ sender: UIButton) {
        ind_load.startAnimating()
        let username = self.in_username.text!
        let password = self.in_password.text!
        
        Alamofire.request(Router.login(username, password: password)!)
            .responseJSON { response in
                if response.response != nil {
                    let status : Int = response.response!.statusCode
                    if status == 200 {
                        let json = JSON(response.result.value!)
                        self.auth = Authentication(user: nil, data: json)
                        
                        let request = Router.getUserDetails(self.auth!, user: username)!
                        Alamofire.request(request).responseJSON { profile_response in
                            let statusCode = profile_response.response!.statusCode
                            if statusCode == 200 {
                                let json = JSON(profile_response.result.value!)
                                let valid = json["User"]["Is Validated"].boolValue
                                let avatar = Avatar(json: json["User"])
                                self.admin = User(name: username, avatar: avatar, is_validated: valid, admin: true)
                                self.auth?.refers_to_user = self.admin
                                
                                if valid {
                                    self.performSegue(withIdentifier: "AcceptLogin", sender: self)
                                    let defaults = UserDefaults.standard
                                    
                                    defaults.setValue(self.in_username.text!, forKey: loginKeys.user)
                                    defaults.setValue(self.in_password.text!, forKey: loginKeys.pass)
                                    defaults.synchronize()
                                    
                                    // Now if device exists, send device
                                    if self.device != nil {
                                        self.registerUserDevice()
                                    }
                                    
                                } else {
                                    // Ask user for confirmation code.
                                    self.confirmScreen.isHidden = false
                                    self.lb_error.isHidden = true
                                }
                            }
                        }
                        
                    } else if status == 401 {
                        self.lb_error.text = "Incorrect username or password"
                        self.lb_error.isHidden = false
                    }
                    self.ind_load.stopAnimating()
                } else {
                    self.lb_error.text = "Sorry, circli server seems to be offline. Try again later!"
                    self.lb_error.isHidden = false
                    self.ind_load.stopAnimating()
                }
        }
    }
    @IBAction func validateUser(_ sender: UIButton) {
        let code = self.in_validationCode.text!
        self.ind_confirm.startAnimating()
        
        Alamofire.request(Router.validateUser(self.auth!, code: code)!).responseJSON { response in
            let status : Int = response.response!.statusCode
            if status == 200 {
                self.ind_confirm.stopAnimating()
                let json = JSON(response.result.value!)
                let valid = json["Profile"]["Is Validated"].boolValue
                
                if valid {
                    self.performSegue(withIdentifier: "AcceptLogin", sender: self)
                    let defaults = UserDefaults.standard
                    
                    defaults.setValue(self.in_username.text!, forKey: loginKeys.user)
                    defaults.setValue(self.in_password.text!, forKey: loginKeys.pass)
                    defaults.synchronize()
                    
                    // Now if device exists, send device
                    if self.device != nil {
                        self.registerUserDevice()
                    }
                } else {
                    self.confirmScreen.isHidden = true
                    self.lb_error.text = "Could not confirm your account. Are you sure the code you sent is correct?"
                    self.lb_error.isHidden = false
                }
                
            }
        }
    }
    @IBAction func resendConfirmationEmail(_ sender: UIButton) {
        self.ind_confirm.startAnimating()
        sender.isEnabled = false
        
        Alamofire.request(Router.resendConfirmationEmail(self.auth!)!).responseJSON { response in
            self.ind_confirm.stopAnimating()
            sender.isEnabled = true
        }
    }
    
    
    // MARK: Misc.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if self.confirmScreen.isHidden {
            return
        }
        
        let touch: UITouch = touches.first!
        if touch.view != self.confirmScreen {
            self.confirmScreen.isHidden = true
        }
    }

    func registerUserDevice() {
        Alamofire.request(Router.registerDevice(auth: auth!, device: device!)!).responseJSON { response in
            let status = response.response?.statusCode
            if status == 200 {
                let json = JSON(response.result.value!)
                if json["Success"].exists() {
                    print("Device successfully registered!")
                }
            }
        }
        
    }
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AcceptLogin" {
            let destinationViewController = segue.destination as! BackgroundViewController
            destinationViewController.auth = self.auth
            destinationViewController.admin = self.admin
        }
        
    }

}
