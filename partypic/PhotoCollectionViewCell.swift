//
//  PhotoCollectionViewCell.swift
//  partypic
//
//  Created by João Henrique Carneiro on 3/12/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    var photo: Photo!
    @IBOutlet weak var img_photo: UIImageView!
    
    func configure() {
        photo.queueImageView(img_photo, completion: {})
    }
}
