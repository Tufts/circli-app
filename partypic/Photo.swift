//
//  Photo.swift
//  partypic
//
//  Created by João Henrique Carneiro on 8/27/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import SwiftyJSON

class Photo : Image {
    
    // MARK: Properties
    var messages: [Message] = []
    var reactions: [Reaction] = []
    var caption: String
    var owner: User

    // MARK: Initialization
    init?(json: JSON) {
        if json["Photo"].exists() {
            self.caption = json["Photo"]["Caption"].rawString()!
            self.owner = User(json: json["Photo"]["Owner"])!
            
            for (_,reaction) in json["Reactions"] {
                self.reactions.append(Reaction(json: reaction))
            }
            for (_,message) in json["Messages"] {
                self.messages.append(Message(json: message))
            }
            super.init(json: json["Photo"], type: "Photo")
        } else {
            return nil
        }
    }
}

class Message {
    
    // MARK: Properties
    var id: Int
    var owner: User
    var content: String
    
    
    // MARK: Initialization
    init?(id: Int, owner: User, message: String) {
        self.id = id
        self.owner = owner
        self.content = message
        
        if content.isEmpty {
            return nil
        }
    }
    
    init(json: JSON) {
        self.id = json["Message"]["Id"].rawValue as! Int
        self.owner = User(json: json["Message"]["Owner"])!
        self.content = json["Message"]["Content"].rawString()!
    }
}

class Reaction {
    
    // MARK: Reaction Properties
    var id: Int
    var owner: User
    var date: String
    var type: String
    
    init(json: JSON) {
        self.id = json["Reaction"]["Id"].rawValue as! Int
        self.owner = User(json: json["Reaction"]["Owner"])!
        self.date = json["Reaction"]["Reaction date"].rawString()!
        self.type = json["Reaction"]["Reaction"].rawString()!
        
    }
}

class Upload {
    static let CAPTURE = 0
    static let LIBRARY = 1
}
