//
//  MenuPhotoTableViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/15/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MenuPhotoTableViewController: UITableViewController {
    
    // MARK: Properties
    var auth: Authentication?
    var group: Group?
    var viewType: ViewType?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "captureImage" {
            let navController = segue.destination as! UINavigationController
            let uploadController = navController.topViewController as! UploadPhotoViewController
            uploadController.mode = Upload.CAPTURE
            uploadController.auth = self.auth
            uploadController.group = self.group
            uploadController.originViewType = self.viewType
        }
        else if segue.identifier == "uploadImage" {
            let navController = segue.destination as! UINavigationController
            let uploadController = navController.topViewController as! UploadPhotoViewController
            uploadController.mode = Upload.LIBRARY
            uploadController.auth = self.auth
            uploadController.group = self.group
            uploadController.originViewType = self.viewType
        }
        else if segue.identifier == "inviteFriendsToGroup" {
            let navController = segue.destination as! UINavigationController
            let uploadController = navController.topViewController as! InviteTableViewController
            uploadController.auth = self.auth
            uploadController.group = self.group
        }
        else if segue.identifier == "leaveGroup" {
            let backgroundController = segue.destination as! BackgroundViewController
            backgroundController.auth = self.auth
            
            Alamofire.request(Router.leaveGroup(auth: self.auth!, group: group!, admin: nil)!).responseJSON { response in
                
            }
        }

    }
    

}
