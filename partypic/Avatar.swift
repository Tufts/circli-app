//
//  Avatar.swift
//  partypic
//
//  Created by João Henrique Carneiro on 3/5/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import Foundation
import SwiftyJSON

class Avatar : Image {
    
    // MARK: Properties

    
    // MARK: Initialization
    init?(json: JSON) {
        let avatar = json["Avatar"]
        if avatar.exists() {
            super.init(json: avatar, type: "Avatar")
        } else {
            return nil
        }
    }
}
