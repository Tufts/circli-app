//
//  BackgroundViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/8/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit

class BackgroundViewController: SWRevealViewController {

    // MARK: Properties
    var auth: Authentication?
    var admin: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "sw_front" {
            let navController = segue.destination as! UINavigationController
            let groupController = navController.topViewController as! GroupTableViewController
            groupController.auth = self.auth
        }
        if segue.identifier == "sw_rear" {
            let menuController = segue.destination as! MenuTableViewController
            menuController.auth = self.auth
            menuController.admin = self.admin
        }
    }

}
