//
//  ppURL.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/10/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import Foundation
import Alamofire

class ppURL {
    //static var domain: String = "http://localhost:8000"
    //static var domain: String = "http://0f21a72d.ngrok.io"
    static var domain: String = "http://ec2-54-186-233-141.us-west-2.compute.amazonaws.com"
    
    static var getToken         = Endpoint(url: "\(domain)/o/token/",                method: "POST")
    static var authenticate     = Endpoint(url: "\(domain)/api/auth/",               method: "POST")
    static var lookupHash       = Endpoint(url: "\(domain)/api/lookup/",             method: "GET")
    static var createGroup      = Endpoint(url: "\(domain)/api/create-group/",       method: "POST")
    static var getGroupContent  = Endpoint(url: "\(domain)/api/group-content/",      method: "GET")
    static var listGroups       = Endpoint(url: "\(domain)/api/list/",               method: "GET")
    static var requestFriend    = Endpoint(url: "\(domain)/api/friend-request/",     method: "POST")
    static var acceptFriend     = Endpoint(url: "\(domain)/api/accept-friend/",      method: "POST")
    static var findUsers        = Endpoint(url: "\(domain)/api/search/",             method: "GET")
    static var listFriends      = Endpoint(url: "\(domain)/api/list-friends/",       method: "GET")
    static var getUserDetails   = Endpoint(url: "\(domain)/api/user/",               method: "GET")
    static var uploadImage      = Endpoint(url: "\(domain)/api/upload-image/",       method: "POST")
    static var uploadAvatar     = Endpoint(url: "\(domain)/api/upload-avatar/",      method: "POST")
    static var inviteToGroup    = Endpoint(url: "\(domain)/api/invite-to-group/",    method: "POST")
    static var checkGroupExists = Endpoint(url: "\(domain)/api/check-group-exists/", method: "POST")
    static var checkUserExists  = Endpoint(url: "\(domain)/api/check-user-exists/",  method: "GET")
    static var createUser       = Endpoint(url: "\(domain)/api/create-user/",        method: "POST")
    static var validateUser     = Endpoint(url: "\(domain)/api/validate-user/",      method: "POST")
    static var resendEmail      = Endpoint(url: "\(domain)/api/resend-email/",       method: "GET")
    static var likePhoto        = Endpoint(url: "\(domain)/api/like-photo/",         method: "POST")
    static var sendMessage      = Endpoint(url: "\(domain)/api/send-message/",       method: "POST")
    static var registerDevice   = Endpoint(url: "\(domain)/api/register-device/",    method: "POST")
    static var updateGroupName  = Endpoint(url: "\(domain)/api/update-group-name/",  method: "POST")
    static var leaveGroup       = Endpoint(url: "\(domain)/api/leave-group/",        method: "POST")
}

/**
 `Endpoint` is a struct that describes a REST API endpoint.
 - **url**: The URL address, including domain and paths, of the API call.
 - **method**: Type of call. Can be one of four: `GET`, `POST`, `UPDATE` or `DELETE`.
 
*/
struct Endpoint {
    var url: String
    var method: String
}

class Router {
    
    fileprivate static func genericRequest(_ endpoint: Endpoint, parameters: [String: String]) -> URLRequest? {
        var urlRequest = URLRequest(url: URL(string: endpoint.url)!)
        urlRequest.httpMethod = endpoint.method
        
        let encodedUrlRequest = try? URLEncoding.default.encode(urlRequest as URLRequestConvertible, with: parameters)
        return encodedUrlRequest
    }
    
    fileprivate static func authenticatedGenericRequest(_ endpoint: Endpoint, parameters: [String: String], auth: Authentication) -> URLRequest? {
        var request = genericRequest(endpoint, parameters: parameters)
        request!.setValue("Bearer \(auth.access_token)", forHTTPHeaderField: "Authorization")
        return request
    }
    
    
    
    // MARK: Endpoint Actions
    
    static func login(_ user: String, password: String) -> URLRequest? {
        let parameters = ["grant_type": "password",
                          "username": user,
                          "password": password,
        ]
        
        var request = genericRequest(ppURL.getToken, parameters: parameters)
        request!.setValue("Basic \(Authentication.client64String)", forHTTPHeaderField: "Authorization")
        return request
    }
    
    static func lookupHash() -> URLRequest? {
        let request = genericRequest(ppURL.lookupHash, parameters: [:])
        return request
    }
    
    static func createGroup(auth: Authentication, hash: String) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "hash": hash,
        ]
        let request = authenticatedGenericRequest(ppURL.createGroup, parameters: parameters, auth: auth)
        return request
    }
    
    static func checkGroupExists(auth: Authentication, hash: String) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "group": hash,
                          ]
        let request = authenticatedGenericRequest(ppURL.checkGroupExists, parameters: parameters, auth: auth)
        return request
    }

    static func listGroups(_ auth: Authentication) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name]
        let request = authenticatedGenericRequest(ppURL.listGroups, parameters: parameters, auth: auth)
        return request
    }
    
    static func getGroupContent(_ auth: Authentication, group: Group) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "hash": group.hash,
        ]
        let request = authenticatedGenericRequest(ppURL.getGroupContent, parameters: parameters, auth: auth)
        return request
    }
    
    static func getUserDetails(_ auth: Authentication, user: String) -> URLRequest? {
        let parameters = ["user": user]
        let request = authenticatedGenericRequest(ppURL.getUserDetails, parameters: parameters, auth: auth)
        return request
    }
    
    static func listFriends(_ auth: Authentication, status: String = "accepted") -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "status": status,
        ]
        
        let request = authenticatedGenericRequest(ppURL.listFriends, parameters: parameters, auth: auth)
        return request
    }
    
    static func checkUserExists(_ query: String) -> URLRequest? {
        let parameters = ["query": query]
        let request = genericRequest(ppURL.checkUserExists, parameters: parameters)
        return request
    }
    
    static func createUser(_ username: String, real_name: String, password: String, email: String) -> URLRequest? {
        let parameters = ["username": username,
                          "real_name": real_name,
                          "password": password,
                          "email": email,
        ]
        let request = genericRequest(ppURL.createUser, parameters: parameters)
        return request
    }
    
    static func validateUser(_ auth: Authentication, code: String) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "code": code,
        ]
        let request = authenticatedGenericRequest(ppURL.validateUser, parameters: parameters, auth: auth)
        return request
    }
    
    static func findUsers(_ auth: Authentication, query: String, type: Int) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "query-type": "\(type)",
                          "query": query,
        ]
        let request = authenticatedGenericRequest(ppURL.findUsers, parameters: parameters, auth: auth)
        return request
    }
    
    static func requestFriend(_ auth: Authentication, requested: String) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "requested-user": requested,
        ]
        
        let request = authenticatedGenericRequest(ppURL.requestFriend, parameters: parameters, auth: auth)
        return request
    }
    
    static func acceptFriend(_ auth: Authentication, accepted: String) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "accepted-user": accepted,
        ]
        
        let request = authenticatedGenericRequest(ppURL.acceptFriend, parameters: parameters, auth: auth)
        return request
    }
    
    static func inviteToGroup(_ auth: Authentication, group: Group, friend: User) -> URLRequest? {
        let parameters = ["invited-by": auth.refers_to_user!.name,
                          "invitee": friend.name,
                          "group": group.hash
                          ]
        
        let request = authenticatedGenericRequest(ppURL.inviteToGroup, parameters: parameters, auth: auth)
        return request
    }
    
    static func likePhoto(_ auth: Authentication, group: Group, photo: Photo) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "group": group.hash,
                          "photo-id": "\(photo.id)",
        ]
        
        let request = authenticatedGenericRequest(ppURL.likePhoto, parameters: parameters, auth: auth)
        return request
    }
    
    static func uploadImage(_ auth: Authentication, group: Group, caption: String, image: UIImage, compression: CGFloat?) -> URLRequest? {
        var compressionLevel : CGFloat
        if compression == nil {
            compressionLevel = 1.0 // Don't compress.
        } else {
            compressionLevel = compression!
        }
        
        let data = UIImageJPEGRepresentation(image, compressionLevel)!
        if data.count > 20000000 { // 20 megabytes
            return nil
        }
        
        
        let imageOrientation : UIImageOrientation = image.imageOrientation
        var rotate : Int = Orientation.UP // In case imageOrientation does not exist, therefore use default UP.
        if imageOrientation == UIImageOrientation.up {
            rotate = Orientation.UP
        } else if imageOrientation == UIImageOrientation.down {
            rotate = Orientation.DOWN
        } else if imageOrientation == UIImageOrientation.left {
            rotate = Orientation.LEFT
        } else if imageOrientation == UIImageOrientation.right {
            rotate = Orientation.RIGHT
        }
        
        let parameters = ["user": auth.refers_to_user!.name,
                          "caption": caption,
                          "group": group.hash,
                          "rotate": String(rotate),
        ]
        
        var request = authenticatedGenericRequest(ppURL.uploadImage, parameters: [:], auth: auth)
        
        
        let date = Date(timeIntervalSinceNow: 0)
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYYMMDDHHmmSS"
        let filePrefix = formatter.string(from: date)
        let fileName = "\(filePrefix)_\(auth.refers_to_user!.name).jpg"
        
        let boundary = randomStringWithLength(70)
        let body = NSMutableData()
        
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request!.setValue(contentType, forHTTPHeaderField:"Content-Type")
        
        for (key, value) in parameters {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name= \"Image\"; filename=\"\(fileName)\"\r\n")
        body.appendString("Content-Type: image/jpg\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")
        
        request!.setValue("\(body.length)", forHTTPHeaderField: "Content-Length")
        request!.httpBody = body as Data
        
        return request
    }
    
    static func uploadAvatar(_ auth: Authentication, image: UIImage, compression: CGFloat?) -> URLRequest? {
        var compressionLevel : CGFloat
        if compression == nil {
            compressionLevel = 1.0 // Don't compress.
        } else {
            compressionLevel = compression!
        }
        
        let data = UIImageJPEGRepresentation(image, compressionLevel)!
        if data.count > 20000000 { // 20 megabytes
            return nil
        }
        
        
        let imageOrientation : UIImageOrientation = image.imageOrientation
        var rotate : Int = Orientation.UP // In case imageOrientation does not exist, therefore use default UP.
        if imageOrientation == UIImageOrientation.up {
            rotate = Orientation.UP
        } else if imageOrientation == UIImageOrientation.down {
            rotate = Orientation.DOWN
        } else if imageOrientation == UIImageOrientation.left {
            rotate = Orientation.LEFT
        } else if imageOrientation == UIImageOrientation.right {
            rotate = Orientation.RIGHT
        }
        
        let parameters = ["user": auth.refers_to_user!.name,
                          "rotate": String(rotate),
                          ]
        
        var request = authenticatedGenericRequest(ppURL.uploadAvatar, parameters: [:], auth: auth)
        
        
        let date = Date(timeIntervalSinceNow: 0)
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYYMMDDHHmmSS"
        let filePrefix = formatter.string(from: date)
        let fileName = "\(filePrefix)_\(auth.refers_to_user!.name).jpg"
        
        let boundary = randomStringWithLength(70)
        let body = NSMutableData()
        
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request!.setValue(contentType, forHTTPHeaderField:"Content-Type")
        
        for (key, value) in parameters {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name= \"Image\"; filename=\"\(fileName)\"\r\n")
        body.appendString("Content-Type: image/jpg\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")
        
        request!.setValue("\(body.length)", forHTTPHeaderField: "Content-Length")
        request!.httpBody = body as Data
        
        return request
    }
    
    static func resendConfirmationEmail(_ auth: Authentication) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name]
        
        let request = authenticatedGenericRequest(ppURL.resendEmail, parameters: parameters, auth: auth)
        return request
    }
    
    static func sendMessage(_ auth: Authentication, message: Message, group: Group, photo: Photo) -> URLRequest? {
        let parameters = ["user": message.owner.name,
                          "content": message.content,
                          "photo": String(photo.id),
                          "group": group.hash,
        ]
        let request = authenticatedGenericRequest(ppURL.sendMessage, parameters: parameters, auth: auth)
        return request
    }
    
    static func registerDevice(auth: Authentication, device: Device) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "token": device.token,
                          "uid": device.uid,
                          "platform": "ios",
        ]
        let request = authenticatedGenericRequest(ppURL.registerDevice, parameters: parameters, auth: auth)
        return request
    }
 
    static func updateGroupName(auth: Authentication, name: String, group: Group) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "group": group.hash,
                          "title": name,
        ]
        let request = authenticatedGenericRequest(ppURL.updateGroupName, parameters: parameters, auth: auth)
        return request
    }
    
    static func leaveGroup(auth: Authentication, group: Group, admin: User?) -> URLRequest? {
        let parameters = ["user": auth.refers_to_user!.name,
                          "group": group.hash,
                          "admin": admin?.name ?? "",
        ]
        let request = authenticatedGenericRequest(ppURL.leaveGroup, parameters: parameters, auth: auth)
        return request
    }
    
    // Auxiliary Functions
    fileprivate static func randomStringWithLength (_ len : Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 1...len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString as String
    }

}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8)
        append(data!)
    }
}
