//
//  GroupViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 7/27/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class GroupViewController: UIViewController {

    // MARK: Properties
    var group: Group?
    var auth: Authentication?
    
    @IBOutlet weak var lb_groupTitle: UILabel!
    @IBOutlet weak var lb_groupHash: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = group?.title
        lb_groupTitle.text = group?.title
        lb_groupHash.text = group?.hash
        
        //loadContent()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    func loadContent() {
        var URLRequest = NSMutableURLRequest(url: URL(string: "\(ppURL.domain)/api/group-content/")!)
        URLRequest.httpMethod = "GET"
        let parameters = ["hash": lb_groupHash.text!]
        let encoding = Alamofire.ParameterEncoding.url
        (URLRequest, _) = encoding.encode(URLRequest, parameters: parameters)
        /*
 
         The above is explained in the link below.
         see - http://stackoverflow.com/questions/39151064/django-oauth2-request-user-returns-anonymoususer
         
        */
        URLRequest.setValue("Bearer \(auth!.access_token)", forHTTPHeaderField: "Authorization")
        
        Alamofire.request(Router.).responseJSON { response in
            let data = JSON(response.result.value!)
            print(data)
        }
    }
     */
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func close() {
        dismiss(animated: true, completion: nil)
    }
}
