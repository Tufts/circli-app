//
//  UploadPhotoViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/13/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UploadPhotoViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    // MARK: Properties
    var bottomConstant: CGFloat?
    var imageOrientation: UIImageOrientation?
    
    
    @IBOutlet weak var img_upload: UIImageView!
    @IBOutlet weak var bt_upload: UIBarButtonItem!
    @IBOutlet weak var in_caption: UITextField!
    @IBOutlet weak var ind_upload: UIActivityIndicatorView!
    @IBOutlet weak var vw_upload: UIView!
    @IBOutlet weak var c_bottom: NSLayoutConstraint!
    
    
    
    var mode: Int?
    var imagePicker: UIImagePickerController!
    var selected: Bool = false
    var auth: Authentication?
    var group: Group?
    var originViewType: ViewType?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bt_upload.isEnabled = false
        self.bottomConstant = self.c_bottom?.constant
        
        NotificationCenter.default.addObserver(self, selector: #selector(UploadPhotoViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UploadPhotoViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if !selected {
            selectImage(self.mode!)
            selected = true
        }
    }
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        textfield.resignFirstResponder()
        return true
    }
    
    func selectImage(_ mode: Int) {
        
        // Probably better to do this camera availability test in the previous view.
        if mode == Upload.CAPTURE {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                takePhotoWithCamera()
            } else {
                let controller = UIAlertController(title: "Sorry", message: "Device has no camera", preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
                    (action : UIAlertAction!) -> Void in
                    
                })
                //let alert: UIAlertView = UIAlertView(title: "Error", message: "Device has no camera", delegate: nil, cancelButtonTitle: "OK")
                //alert.show()
                
                controller.addAction(cancelAction)
                self.present(controller, animated: true, completion: nil)
            }
        } else if mode == Upload.LIBRARY {
            selectImageFromLibrary()
        }
    }

    func takePhotoWithCamera() {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func selectImageFromLibrary() {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            img_upload.image = image
            self.imageOrientation = image.imageOrientation
            bt_upload.isEnabled = true
        } else {
            print("Something wrong")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: Keyboard movements.

    func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = ((info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue)!
        self.c_bottom?.constant = keyboardFrame.size.height + 20
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.c_bottom?.constant = self.bottomConstant!
    }
    
    // MARK: - Navigation
    @IBAction func uploadToGroup(_ sender: UIButton) {
        sender.isEnabled = false
        vw_upload?.isHidden = false
        ind_upload?.startAnimating()
        let request = Router.uploadImage(auth!, group: group!, caption: in_caption.text!, image: img_upload.image!, compression: 0.2)
        if request != nil { // Image too big.
            Alamofire.request(request!)
                .responseJSON { response in
                    print(response)
                    self.bt_upload.isEnabled = true
                    self.ind_upload?.stopAnimating()
                    self.vw_upload?.isHidden = true
                    self.performSegue(withIdentifier: "showGroup", sender: self)
            }
        } // Would be ideal to put an ELSE here, warning user that image is too big.
    }
    
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showGroup" {
            let groupMainBackground = segue.destination as! GroupMainBackground
            groupMainBackground.group = self.group
            groupMainBackground.auth = self.auth
            groupMainBackground.viewtype = self.originViewType!
        }
    }
}
