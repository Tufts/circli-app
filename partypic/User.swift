//
//  User.swift
//  partypic
//
//  Created by João Henrique Carneiro on 8/30/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import SwiftyJSON

class User {
    
    var name: String
    var avatar: Avatar?
    var admin: Bool
    var is_validated: Bool?
    var device: Device?
    
    init?(name: String, avatar: Avatar? = nil, is_validated: Bool?, admin: Bool = false) {
        self.name = name
        self.avatar = avatar
        self.is_validated = is_validated
        self.admin = admin
        
        if name.isEmpty {
            return nil
        }
    }
    
    init?(json: JSON, device: Device? = nil, admin: Bool = false) {
        self.name = json["Name"].rawString()!
        self.is_validated = json["Name"].boolValue
        self.avatar = Avatar(json: json)
        self.admin = admin
        self.device = device
        
        if self.name.isEmpty {
            return nil
        }
    }
    
    /*
    func updateAvatar(_ new: String) {
        self.avatar = new
        
    }
     */
}

class Device {
    var token: String
    var uid: String
    
    init?(token: String, uid: String) {
        if token.isEmpty || uid.isEmpty {
            return nil
        }
        self.token = token
        self.uid = uid
    }
}
