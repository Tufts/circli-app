//
//  AppDelegate.swift
//  partypic
//
//  Created by João Henrique Carneiro on 7/15/16.
//  Copyright (c) 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import UserNotifications
// import Firebase
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Customization options.
        defineNavigationBar()
        
        // Register at launch for remote notificaitons.
        registerForPush(application)
        
        // Register Firebase.
        //FIRApp.configure()
        
        return true
    }
    
    // MARK: Push Notification Registration
    func registerForPush(_ application: UIApplication) {
        // Types of notifications: Customize here if necessary.
        let types: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: types) { (granted, error) in
            // Enable or disable features based on authorization
        }
        
        application.registerForRemoteNotifications()
    }
    
    // MARK: Push Notification Delegate Methods
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var uid: String
        if UserDefaults.standard.object(forKey: "uid") != nil {
            uid = UserDefaults.standard.string(forKey: "uid")!
        } else {
            uid = UUID().uuidString
            UserDefaults.standard.setValue(uid,forKey: "uid")
            UserDefaults.standard.synchronize()
        }
        
        let token = deviceToken as NSData
        let device: Device = Device(token: token.description, uid: uid)!
        let loginController = application.keyWindow!.rootViewController as! LoginViewController
        loginController.device = device
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        // First cast userInfo["aps"] to a usable dictionary object.
        let aps = userInfo["aps"] as! [String: AnyObject]
        print(aps)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Got a notification while in foreground")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Got a notification while in background")
    }

    // MARK: Other application methods
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    // MARK: App Customization
    func defineNavigationBar() {
        let nav = UINavigationBar.appearance()
        nav.tintColor =  UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
        nav.barTintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
        nav.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
    }
    

}

