//
//  PhotoFlowLayout.swift
//  partypic
//
//  Created by João Henrique Carneiro on 3/12/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import UIKit

class PhotoFlowLayout: UICollectionViewFlowLayout {

    override var itemSize: CGSize {
        set {
            
        }
        get {
            let numberOfColumns: CGFloat = 3
            let itemWidth = (self.collectionView!.frame.width - (numberOfColumns - 1)) / numberOfColumns
            return CGSize(width: itemWidth, height: itemWidth)
        }
    }
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 1
        minimumLineSpacing = 1
        scrollDirection = .vertical
    }
}
