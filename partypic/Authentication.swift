//
//  Authentication.swift
//  partypic
//
//  Created by João Henrique Carneiro on 8/27/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import SwiftyJSON

class Authentication {
    
    // MARK: Properties
    /* Use the clientdId and Secret below if on mac server.
    static let clientId: String = "KH8rtXLonWUoZyn6QdqB7wn60hFDWLYRNUlS05Tc"
    static let clientSecret: String = "6B72QWw7K63AHHDY51vdywnYvTCGAmILqC87MQPAYE3zGkqLA4v8zvVj104DzfkrRpeyj7pwZbmaJOSPRXCRXTCMU9VgFffDH7kbDtgf0ETcJqnydtxZGbSCkUDOhzoS"
    */
    //For AWS, use below:
    
    static let clientId: String = "XkGxqfGcqPRHvopWsNY2WNzAUMKiLu7UyWiuMRPY"
    static let clientSecret: String = "5sQXqmdN9bgIXEPXBgIanYuILBRDzaZ4GxsGfcwCp3kROGy0w5a5J5TuwjHKny9dRLseWB4yCElnWgnB7vMmgohynzHHsJHAY4eKr7wckpdYqa62UR8HDBUeIiLodDWc"
 
    
    static let clientData: Data = "\(clientId):\(clientSecret)".data(using: String.Encoding.utf8)!
    static let client64String = clientData.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
    
    // MARK: Class user variables
    var refers_to_user: User?
    var access_token: String
    var refresh_token: String
    var valid_to: Date?
    
    init?(user: User?, access_token: String, refresh_token: String, valid_to: Date?) {
        self.refers_to_user = user
        self.access_token = access_token
        self.refresh_token = refresh_token
        self.valid_to = valid_to
        
        if access_token.isEmpty || refresh_token.isEmpty {
            return nil
        }
    }
    
    init?(user: User?, data: JSON) {
        self.refers_to_user = user
        self.access_token = data["access_token"].rawString()!
        self.refresh_token = data["refresh_token"].rawString()!
        let expiration_seconds = data["expires_in"].rawValue as! Int
        self.valid_to = (Calendar.current as NSCalendar).date(
            byAdding: [.second],
            value: expiration_seconds,
            to: Date(),
            options: []
            )!
    }
}
