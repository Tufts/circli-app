//
//  GroupTableViewCell.swift
//  partypic
//
//  Created by João Henrique Carneiro on 7/19/16.
//  Copyright (c) 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class GroupTableViewCell: UITableViewCell {

    // MARK: Outlets
    @IBOutlet weak var lb_groupTitle: UILabel!
    @IBOutlet weak var img_groupAvatar: UIImageView!
    @IBOutlet weak var lb_groupHash: UILabel!
    @IBOutlet weak var lb_groupMembers: UILabel!
    
    // MARK: Properties
    var group: Group!
    var inHiddenMode: Bool = false {
        didSet {
            lb_groupHash.isHidden = !inHiddenMode
        }
    }
    
    // MARK: Gestures
    var pan: UIPanGestureRecognizer?
    var endtap: UITapGestureRecognizer?
    
    
    // MARK: Cell methods
    override func awakeFromNib() {
        super.awakeFromNib()
        inHiddenMode = false
        
        let height = img_groupAvatar.frame.height
        img_groupAvatar.layer.cornerRadius = height/2.5;
        img_groupAvatar.layer.masksToBounds = true;
        
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(GroupTableViewCell.enterHiddenMode(sender:)))
        longpress.numberOfTouchesRequired = 2
        self.contentView.addGestureRecognizer(longpress)
    }
    
    func enterHiddenMode(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.began {
            //pan = UIPanGestureRecognizer(target: self, action: #selector(GroupTableViewCell.leaveGroup(sender:)))
            //self.contentView.addGestureRecognizer(pan!)
        }
        if (sender.state == UIGestureRecognizerState.ended) {
            endtap = UITapGestureRecognizer(target: self, action: #selector(GroupTableViewCell.exitHiddenMode(sender:)))
            self.window?.addGestureRecognizer(endtap!)
            return
        }
        
        self.inHiddenMode = true
    }

    func exitHiddenMode(sender: UITapGestureRecognizer) {
        print("Exiting Hidden Mode")
        self.inHiddenMode = false
        self.window?.removeGestureRecognizer(endtap!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
