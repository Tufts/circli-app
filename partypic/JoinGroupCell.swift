//
//  JoinGroupCell.swift
//  partypic
//
//  Created by João Henrique Carneiro on 3/5/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class JoinGroupCell: UITableViewCell {
    
    var auth: Authentication!
    @IBOutlet weak var in_group: UITextField!
    @IBOutlet weak var bt_join: UIButton!
    
    var handler: FormHandler?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        in_group.addTarget(self, action: #selector(JoinGroupCell.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        handler = FormHandler(inputs: [in_group], completion: autoAddGroup)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldDidChange(_ textfield: UITextField) {
        if in_group.text!.isEmpty {
            self.bt_join.isEnabled = false
        } else {
            self.bt_join.isEnabled = true
        }
    }
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        handler?.handle(textfield: textfield)
        return true
    }
    
    func autoAddGroup() {
        addExistingGroup(bt_join)
    }
    
    @IBAction func addExistingGroup(_ sender: UIButton) {
        var hash: String
        
        if self.in_group.text != nil {
            hash = self.in_group.text!
            sender.setTitle("Joining...", for: .disabled)
        } else {
            return
        }
        
        Alamofire.request(Router.checkGroupExists(auth: self.auth, hash: hash)!).responseJSON { response in
            let status = response.response?.statusCode
            if status == 200 {
                let json = JSON(response.result.value!)
                self.in_group.text = ""
                sender.setTitle("Join", for: .disabled)
                if json["Success"].exists() {
                    // WORKED - Notify the system to refresh the GroupTable.
                    NotificationCenter.default.post(name: NSNotification.Name("refresh"), object: nil)
                } else {
                    // COULD NOT ADD TO GROUP.
                }
            } else {
                // BAD NETWORK RESPONSE
                sender.isEnabled = true
            }
            
        }
    }
    
}
