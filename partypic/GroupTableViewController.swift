//
//  GroupTableViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 7/22/16.
//  Copyright (c) 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


private enum Section: Int {
    case joinGroupSection = 0
    case groupSection = 1
}

class GroupTableViewController: UITableViewController {
    
    // MARK: Properties
    var auth: Authentication?
    var groups = [Group?]()
    var contextMenu = false
    
    @IBOutlet weak var bt_menu: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            bt_menu.target = self.revealViewController()
            bt_menu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        loadGroups()
        NotificationCenter.default.addObserver(self, selector: #selector(GroupTableViewController.refreshList(notification:)), name: Notification.Name("refresh"), object: nil)
    }
    
    func loadGroups() {
        Alamofire.request(Router.listGroups(self.auth!)!).responseJSON { response in
            if response.result.value != nil {
            let json = JSON(response.result.value!)
                if json["Groups"].count > 0 {
                    for i in 0...(json["Groups"].count-1) {
                        var g = json["Groups"][i]
                        let titletext = g["Group"]["Title"].rawString()!
                        var title: String?
                        if titletext.isEmpty {
                            title = nil
                        } else {
                            title = titletext
                        }
                        
                        let hash = g["Group"]["Hash"].rawString()!
                        let members = g["Group"]["Members"]
                        let avatar = Photo(json: g["Group"])
                        
                        
                        var mems: [String] = []
                        for j in 0...(members.count-1) {
                            let m = json["Groups"][i]["Group"]["Members"][j]["Name"].rawString()!
                            mems.append(m)
                        }
                        
                        let g_ = Group(title, hash: hash, members: mems, avatar: avatar)
                        
                        if g_ != nil {
                            self.groups.append(g_)
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let currentSection: Section = Section(rawValue: section) {
            switch currentSection {
            case .joinGroupSection:
                return 1
            case .groupSection:
                return groups.count
            }
        } else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = indexPath.section == Section.joinGroupSection.rawValue ? "joinGroupCell" : "groupCell"

        let genericCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        if cellIdentifier == "groupCell" {
            let cell = genericCell as! GroupTableViewCell
            
            let group = groups[indexPath.row]!
            
            cell.lb_groupTitle.text = group.title ?? group.hash
            cell.lb_groupHash.text = group.hash
            cell.group = group
            
            if group.avatar != nil {
                group.avatar!.queueImageView(cell.img_groupAvatar, completion: {})
            } else {
                cell.img_groupAvatar.image = UIImage(named: "Icon-App-83.5x83.5.png")
            }
            
            cell.lb_groupMembers.text = group.members.joined(separator: ", ")
            
            return cell
        } else if cellIdentifier == "joinGroupCell" {
            let cell = genericCell as! JoinGroupCell
            cell.auth = self.auth!
            return cell
        } else {
            return genericCell
        }
    }

    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            groups.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.delete
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let leaveAction = UITableViewRowAction(style: UITableViewRowActionStyle.destructive, title: "Leave Group") { (action, indexPath) -> Void in
            let group = self.groups[indexPath.row]
            self.leaveGroup(group: group!, indexPath: indexPath)
        }
        return [leaveAction]
    }
    
    // Make sure the first cell (add group form), is not editable.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if let currentSection: Section = Section(rawValue: indexPath.section) {
            switch currentSection {
                case .joinGroupSection:
                    return false
                case .groupSection:
                    return true
            }
        } else {
            return false
        }
    }
    
    
    
    
    
    
    
    /*
     refreshList - Reloads group data in tableview. This function will be called by child view controllers, via the notifications system.
    */
    func refreshList(notification: Notification) {
        groups = [Group?]()
        self.loadGroups()
    }
    
    func leaveGroup(group: Group, indexPath: IndexPath) {
        let auth = self.auth!
        
        Alamofire.request(Router.leaveGroup(auth: auth, group: group, admin: nil)!).responseJSON { response in
            let status = response.response?.statusCode
            if status == 200 {
                let json = JSON(response.result.value!)
                if json["Success"].exists() {
                    // We have sucessfully left the group!
                    self.groups.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                }
            }
        }
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowGroup" {
            let groupMainBackground = segue.destination as! GroupMainBackground
            
            // Get the cell that generated this segue.
            if let selectedGroupCell = sender as? GroupTableViewCell {
                let indexPath = tableView.indexPath(for: selectedGroupCell)!
                let selectedGroup = groups[indexPath.row]
                groupMainBackground.group = selectedGroup
                groupMainBackground.auth = self.auth
            }
        } else if segue.identifier == "AddGroup" {
            let navController = segue.destination as! UINavigationController
            let newGroupController = navController.topViewController as! NewGroupViewController
            newGroupController.auth = self.auth
        }
    }
}
