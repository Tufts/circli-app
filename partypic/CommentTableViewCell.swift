//
//  CommentTableViewCell.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/12/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    // MARK: Properties
    var message: Message!
    
    // MARK: Storyboard elements
    @IBOutlet weak var lb_owner: UILabel!
    @IBOutlet weak var lb_comment: UILabel!
    @IBOutlet weak var img_avatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure() {
        let height = img_avatar.frame.height
        let width = img_avatar.frame.width
        img_avatar.layer.cornerRadius = max(width,height)/2
        img_avatar.layer.masksToBounds = true;
        lb_owner.text = message.owner.name
        lb_comment.text = message.content
        message.owner.avatar?.queueImageView(img_avatar, completion: {})
    }

}
