//
//  FriendRequestsTableViewCell.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/10/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FriendRequestsTableViewCell: UITableViewCell {

    // MARK: Properties
    var auth: Authentication?
    var selectedRow: Int?
    weak var cellController: FriendRequestsTableViewController!
    @IBOutlet weak var lb_username: UILabel!
    @IBOutlet weak var lb_userRL: UILabel!
    @IBOutlet weak var img_avatar: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        img_avatar.layer.cornerRadius = 25
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func acceptFriendRequest(_ sender: AnyObject) {
        let accepted = lb_username.text!
        
        Alamofire.request(Router.acceptFriend(auth!, accepted: accepted)!).responseJSON { response in
            let status : Int = response.response!.statusCode
            if status == 200 {
                let json = JSON(response.result.value!)
                
                if json["Message"].exists() {
                    print("Tell user that friend request has been accepted.")
                    self.cellController.requests.remove(at: self.selectedRow!)
                    self.cellController.tableView.reloadData()
                }
            }
        }
    }

}
