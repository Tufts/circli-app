//
//  DetailedPhotoViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/8/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

private enum Section: Int {
    case imageSection = 0
    case commentSection = 1
}

class DetailedPhotoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    // MARK: Properties
    var group: Group?
    var photo: Photo?
    var auth: Authentication?
    var bottomConstant: CGFloat?
    
    //var image: UIImage?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var in_comment: UITextField!
    @IBOutlet weak var c_bottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedPhotoViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DetailedPhotoViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        self.bottomConstant = self.c_bottom.constant
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func loadData() {
        Alamofire.request(Router.listFriends(self.auth!, status: "pending")!).responseJSON { response in
            let data = JSON(response.result.value!)
            print(data)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: TableView Protocol Conform Functions
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let currentSection: Section = Section(rawValue: section) {
            switch currentSection {
            case .imageSection:
                return 1
            case .commentSection:
                return photo!.messages.count
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = indexPath.section == Section.imageSection.rawValue ? "ImageCell" : "CommentCell"
        let genericCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        if cellIdentifier == "ImageCell"{
            let cell = genericCell as! PhotoTableViewCell
            cell.auth = auth!
            cell.group = group!
            cell.photo  = photo!
            cell.configure()
            return cell
            
        } else if cellIdentifier == "CommentCell" {
            let cell = genericCell as! CommentTableViewCell
            cell.message = photo!.messages[indexPath.row]
            cell.configure()
            return cell
        } else {
            return genericCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
            if section == Section.imageSection.rawValue {
            var height: CGFloat
            
            let width = UIScreen.main.bounds.size.width
            let orientation = photo!.orientation
            let aspect = CGFloat(photo!.aspect)
            
            if orientation == Orientation.UP || orientation == Orientation.DOWN {
                // Aspect ratio is correct
                height = width*aspect
            } else {
                // Inverse!
                height = width*(1/aspect)
            }
            
            return height + 62 // Very dirty. Feels hacky.
            } else {
                tableView.estimatedRowHeight = 140
                let height = UITableViewAutomaticDimension
                return height
        }
    }
    
    // MARK: Keyboard RETURN key implementations
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        let m = Message(id: 0, owner: self.auth!.refers_to_user!, message: textfield.text!)
        
        if m != nil {
            sendMessage(m!)
            
        }
        textfield.text = ""
        
        textfield.resignFirstResponder()
        return true
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = ((info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue)!
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.c_bottom.constant = keyboardFrame.size.height + 20
        })
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.c_bottom.constant = self.bottomConstant!
    }

    // MARK: Network Calls
    func sendMessage(_ message: Message) {
        Alamofire.request(Router.sendMessage(auth!, message: message, group: group!, photo: photo!)!).responseJSON { response in
            let status = response.response?.statusCode
            
            if status == 200 {
                let json = JSON(response.result.value!)
                if json["Success"].exists() {
                    self.photo!.messages.append(message)
                    self.tableView.reloadSections(IndexSet(integer: Section.commentSection.rawValue), with: UITableViewRowAnimation.automatic)
                }
            }
        }
    }
}
