//
//  FriendTableViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 8/30/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FriendTableViewController: UITableViewController, UISearchBarDelegate, UISearchDisplayDelegate {

    var friends = [User?]()
    var auth: Authentication?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFriendsList()
    }

    func loadFriendsList() {
        Alamofire.request(Router.listFriends(auth!, status: "accepted")!)
            .responseJSON { response in
                let status : Int = response.response!.statusCode
                if status == 200 {
                    let data = JSON(response.result.value!)
                    let relationships = data["Relationships"]
                    
                    for (_, r):(String, JSON) in relationships {
                        let user1 = r["Relationship"]["User One"].rawString()!
                        let user2 = r["Relationship"]["User Two"].rawString()!
                        var user: String
                        if self.auth!.refers_to_user!.name == user1 {
                            user = user1
                        } else {
                            user = user2
                        }
                        
                        Alamofire.request(Router.getUserDetails(self.auth!, user: user)!)
                            .responseJSON { response in
                                let status : Int = response.response!.statusCode
                                let data = JSON(response.result.value!)
                                if status == 200 {
                                    let avatar = Avatar(json: data["User"]["Avatar"])
                                    self.friends.append(User(name: user, avatar: avatar, is_validated: nil))
                                }
                        }
                    }
                }
                self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return friends.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "FriendTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FriendTableViewCell
        
        let friend = friends[indexPath.row]
        cell.lb_username.text = friend?.name
        cell.lb_realLife.text = friend?.name
        
        let avatar = friend?.avatar
        if avatar != nil {
            avatar!.queueImageView(cell.img_avatar, completion: {})
        }
        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
