//
//  PhotoCollectionViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 3/12/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

private let reuseIdentifier = "PhotoCell"

class PhotoCollectionViewController: UICollectionViewController {

    // MARK: Properties
    var auth: Authentication?
    var group: Group?
    var photos: [Photo] = []
    
    @IBOutlet weak var bt_menu: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = group?.title ??  group?.hash

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        //self.collectionView!.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        if self.revealViewController() != nil {
            self.revealViewController().rightViewRevealWidth = 200
            bt_menu.target = self.revealViewController()
            bt_menu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            let menu = self.revealViewController().rightViewController as! MenuPhotoTableViewController
            menu.viewType = ViewType.Grid
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshTitle(notification:)), name: Notification.Name("refresh-title"), object: nil)
        loadPhotos()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func refreshTitle(notification: Notification) {
        self.navigationItem.title = group?.title ?? group?.hash
    }

    func loadPhotos() {
        Alamofire.request(Router.getGroupContent(self.auth!, group: self.group!)!).responseJSON { response in
            let data = JSON(response.result.value!)
            for (_, entry):(String, JSON) in data["Photos"] {
                let photo = Photo(json: entry)
                self.photos.append(photo!)
            }
            self.collectionView?.reloadData()
        }
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return photos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCollectionViewCell
    
        // Configure the cell
        cell.photo = photos[indexPath.row]
        cell.configure()
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
    func updateGroupName(name: String) {
        Alamofire.request(Router.updateGroupName(auth: auth!, name: name, group: group!)!).responseJSON { response in
            let status = response.response?.statusCode
            if status == 200 {
                let json = JSON(response.result.value!)
                if json["Success"].exists() {
                    // Group title succesfully changed!
                    self.group?.title = name
                    NotificationCenter.default.post(name: NSNotification.Name("refresh-title"), object: nil)
                    //self.navigationItem.title = name
                } else {
                    // Oops, probably server error. Could not change name.
                    // Should send a message to user about it.
                }
            }
        }
    }
    
    @IBAction func changeGroupName(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Change the Group Name",
                                                message: "",
                                                preferredStyle: UIAlertControllerStyle.alert
        )
        
        let saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.default,
                                       handler: { alert -> Void in
                                        
                                        let groupName = alertController.textFields![0] as UITextField
                                        
                                        if groupName.text != nil && groupName.text!.characters.count > 0 {
                                            self.updateGroupName(name: groupName.text!)
                                        }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Let's call it"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ReloadBackground" {
            let backgroundController = segue.destination as! BackgroundViewController
            backgroundController.auth = self.auth
        } else if segue.identifier == "ShowMessages" {
            let indexPath : IndexPath
            if let cell = sender as? UICollectionViewCell {
                indexPath = (self.collectionView?.indexPath(for: cell)!)!
                let selectedPhoto = photos[indexPath.row]
                let detailController = segue.destination as! DetailedPhotoViewController
                detailController.auth = self.auth
                detailController.photo = selectedPhoto
                detailController.group = self.group
            }
        }
    }

}
