//
//  _FormViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 1/26/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import UIKit

class FormHandler {

    var form: [UITextField]
    var activeInput: UITextField?
    var completion: () -> Void

    init?(inputs: [UITextField], completion: @escaping () -> Void) {
        if inputs.count == 0 {
            return nil
        }
        self.form = inputs
        self.completion = completion
        for input in self.form {
            input.addTarget(self, action: #selector(FormHandler.setActiveField(_:)), for: UIControlEvents.editingDidBegin)
        }
        
    }
    
    func handle(textfield: UITextField) {
        if textfield.tag < form.count - 1 {
            self.nextInput()
        } else {
            self.finishInput()
        }
    }
    
    @objc func setActiveField(_ textfield: UITextField) {
        self.activeInput = textfield
    }
    
    func nextInput() {
        self.form[self.activeInput!.tag + 1].becomeFirstResponder()
    }
    
    func previousInput() {
        self.form[self.activeInput!.tag - 1].becomeFirstResponder()
    }
    
    func finishInput() {
        self.form[self.activeInput!.tag].resignFirstResponder()
        self.completion()
    }
}
