//
//  ViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 7/15/16.
//  Copyright (c) 2016 João Henrique Carneiro. All rights reserved.
//

import Alamofire
import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    // MARK: Properties
    
    @IBOutlet weak var in_groupAdd: UITextField!
    @IBOutlet weak var lb_result: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Actions
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        lb_result.text = textField.text
        textField.text = ""
    }
}