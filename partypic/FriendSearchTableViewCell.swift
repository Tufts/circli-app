//
//  FriendSearchTableViewCell.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/26/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FriendSearchTableViewCell: UITableViewCell {
    
    // MARK: Properties
    var auth: Authentication?
    @IBOutlet weak var lb_username: UILabel!
    @IBOutlet weak var img_avatar: UIImageView!
    @IBOutlet weak var lb_requestResponse: UILabel!
    //@IBOutlet weak var view_messageBox: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let height = img_avatar.frame.height
        img_avatar.layer.cornerRadius = height/2;
        img_avatar.layer.masksToBounds = true;
        
        // This should already be hidden, but let's make sure.
        lb_requestResponse.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func sendFriendRequest(_ sender: UIButton) {
        sender.isEnabled = false
        let username = lb_username.text!
        self.lb_requestResponse.text = "..."
        self.lb_requestResponse.isHidden = false
        
        Alamofire.request(Router.requestFriend(self.auth!, requested: username)!).responseJSON { response in
            let status : Int = response.response!.statusCode
            if status == 200 {
                let json = JSON(response.result.value!)
                
                if json["Message"].exists() {
                   self.lb_requestResponse.text = "Friend request sent!"
                } else {
                    // Should say something if something wrong happens!
                    sender.isEnabled = true
                    self.lb_requestResponse.text = "Could not send friend request."
                }
            } else {
                // Wrong here as well!
                sender.isEnabled = true
                self.lb_requestResponse.text = "No response from server. Try again later."
            }
        }
    }

}
