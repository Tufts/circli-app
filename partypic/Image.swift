//
//  Image.swift
//  partypic
//
//  Created by João Henrique Carneiro on 3/5/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import Foundation
import SwiftyJSON

class Image {
    var id: Int
    var url: String
    var image: UIImage?
    var date: Date?
    var aspect: Float
    var orientation: Int
    var preview: String?
    var type: String
    
    // MARK: Initialization
    init?(json: JSON, type: String) {
        self.id = json["Id"].rawValue as! Int
        self.url = "\(ppURL.domain)\(json["Url"].rawString()!)"
        self.aspect = json["Aspect"].rawValue as! Float
        self.orientation = json["Orientation"].rawValue as! Int
        self.type = type
        
        if json["Preview"].exists() {
            self.preview = json["Preview"].rawString()
        }
    }
    
    func queueImageView(_ view: UIImageView, completion: @escaping () -> Void) {
        var url: URL
        if self.preview != nil {
            url = URL(string: "\(ppURL.domain)\(self.preview!)")!
        } else {
            url = URL(string: self.url)!
        }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (responseData, responseUrl, error) -> Void in
            if let data = responseData {
                // Execute in UI thread
                DispatchQueue.main.async(execute: { () -> Void in
                    view.image = UIImage(data: data)
                    completion()
                })
            }
        })
        task.resume()
    }
}
