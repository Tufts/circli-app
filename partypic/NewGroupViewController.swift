//
//  GroupViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 7/22/16.
//  Copyright (c) 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NewGroupViewController: UIViewController {

    // MARK: Properties
    var auth: Authentication?
    var group : Group?
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var in_addGroup: UITextField!
    @IBOutlet weak var lb_groupHash: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        saveButton.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Hash Generation
    @IBAction func newGroup(_ sender: AnyObject) {
        Alamofire.request(Router.lookupHash()!).responseJSON { response in // 1
            let test = JSON(response.result.value!)
            print(test)
            let json = JSON(response.result.value!)
            let hash = json["Hash"]
            self.lb_groupHash.text = "\(hash)"
        }
        saveButton.isEnabled = true
    }
    
    
    @IBAction func createGroup(_ sender: UIBarButtonItem) {
        sender.isEnabled = false
        let hash = self.lb_groupHash.text!
        Alamofire.request(Router.createGroup(auth: self.auth!, hash: hash)!).responseJSON { response in
            let status = response.response?.statusCode
            if status == 200 {
                let json = JSON(response.result.value!)
                if json["Success"].exists() {
                    // Group was created.
                    NotificationCenter.default.post(name: NSNotification.Name("refresh"), object: nil)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func addExistingGroup(_ sender: UIButton) {
        var hash: String
        
        if self.in_addGroup.text != nil {
            hash = self.in_addGroup.text!
            sender.isEnabled = false
        } else {
            return
        }
        
        Alamofire.request(Router.checkGroupExists(auth: self.auth!, hash: hash)!).responseJSON { response in
            let status = response.response?.statusCode
            if status == 200 {
                // WORKED.
                let json = JSON(response.result.value!)
                if json["Success"].exists() {
                    self.saveButton.isEnabled = true
                } else {
                    sender.isEnabled = true
                }
            } else {
                // DIDNT WORK
                sender.isEnabled = true
            }
            
        }
    }
    

    // MARK: - Navigation

    @IBAction func cancel(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if saveButton == sender as! UIBarButtonItem {
            let title = ""
            let hash = lb_groupHash.text!
            let members = ["me"]
            
            group = Group(title, hash: hash, members: members, avatar: nil)
        }
    }
 */

}
