//
//  MenuTableViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 9/7/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MenuTableViewController: UITableViewController {

    // MARK: Properties
    var requests = [User?]()
    @IBOutlet weak var ob_notification: UIView!
    @IBOutlet weak var lb_notificationNumber: UILabel!
    
    var auth: Authentication?
    var admin: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ob_notification.layer.cornerRadius = 10
        ob_notification.isHidden = true
        lb_notificationNumber.isHidden = true
        self.tableView.isScrollEnabled = false
        
        loadFriendRequests()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    /*
    override func viewWillAppear(_ animated: Bool) {
        
        let frontView = try? self.revealViewController().frontViewController as? UINavigationController
        let groupView = try? frontView??.topViewController as? GroupTableViewController
        
        if groupView != nil {
            for cell in (groupView??.tableView.visibleCells)! {
                cell.isUserInteractionEnabled = false
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let frontView = try? self.revealViewController().frontViewController as? UINavigationController
        let groupView = try? frontView??.topViewController as? GroupTableViewController
        
        if groupView != nil {
            for cell in (groupView??.tableView.visibleCells)! {
                cell.isUserInteractionEnabled = false
            }
        }
    }
 */
    
    func loadFriendRequests() {
        Alamofire.request(Router.listFriends(auth!, status: "pending")!)
            .responseJSON { response in
                let status : Int = response.response!.statusCode
                if status == 200 {
                    let data = JSON(response.result.value!)
                    let relationships = data["Relationships"]
                    
                    if relationships.count > 0 {
                        self.lb_notificationNumber.text = "\(relationships.count)"
                        self.ob_notification.isHidden = false
                        self.lb_notificationNumber.isHidden = false
                    }
                    
                    for (_, r):(String, JSON) in relationships {
                        let user = r["Relationship"]["Last Action By"].rawString()!
                        Alamofire.request(Router.getUserDetails(self.auth!, user: user)!)
                            .responseJSON { response in
                                let status : Int = response.response!.statusCode
                                let data = JSON(response.result.value!)
                                if status == 200 {
                                    let avatar = Avatar(json: data["User"]["Avatar"])
                                    self.requests.append(User(name: user, avatar: avatar, is_validated: nil))
                                }
                        }
                    }
                }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    /*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    */
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SearchFriend" {
            let navController = segue.destination as! UINavigationController
            let friendController = navController.topViewController as! FriendSearchViewController
            
            friendController.auth = self.auth
        }
        else if segue.identifier == "FriendRequests" {
            let navController = segue.destination as! UINavigationController
            let requestsController = navController.topViewController as! FriendRequestsTableViewController
            requestsController.requests = self.requests
            requestsController.auth = self.auth
        }
        else if segue.identifier == "GroupTableFromMenu" {
            let navController = segue.destination as! UINavigationController
            let groupTableController = navController.topViewController as! GroupTableViewController
            groupTableController.auth = self.auth
        }
        else if segue.identifier == "ShowFriends" {
            let navController = segue.destination as! UINavigationController
            let friendController = navController.topViewController as! FriendTableViewController
            friendController.auth = self.auth
        }
        else if segue.identifier == "ShowProfile" {
            let navController = segue.destination as! UINavigationController
            let profileController = navController.topViewController as! ProfileViewController
            profileController.auth = self.auth
            profileController.admin = self.admin
        }
        else if segue.identifier == "logout" {
            let defaults = UserDefaults.standard
            
            defaults.setValue("", forKey: loginKeys.user)
            defaults.setValue("", forKey: loginKeys.pass)
            defaults.synchronize()
        }
    }

}
