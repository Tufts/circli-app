//
//  PhotoTableViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 8/27/16.
//  Copyright © 2016 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PhotoTableViewController: UITableViewController {

    // MARK: Properties
    var auth: Authentication?
    var group: Group?
    var photos: [Photo] = []
    
    @IBOutlet weak var bt_menu: UIBarButtonItem!
    @IBOutlet weak var bt_back: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = group?.title ??  group!.hash
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshTitle(notification:)), name: Notification.Name("refresh-title"), object: nil)
        
        if self.revealViewController() != nil {
            self.revealViewController().rightViewRevealWidth = 200
            bt_menu.target = self.revealViewController()
            bt_menu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            let menu = self.revealViewController().rightViewController as! MenuPhotoTableViewController
            menu.viewType = ViewType.Feed
        }
        
        loadPhotos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func refreshTitle(notification: Notification) {
        self.navigationItem.title = group?.title ?? group?.hash
    }
    
    func loadPhotos() {
        Alamofire.request(Router.getGroupContent(self.auth!, group: self.group!)!).responseJSON { response in
            let data = JSON(response.result.value!)
            for (_, entry):(String, JSON) in data["Photos"] {
                let photo = Photo(json: entry)
                self.photos.append(photo!)
            }
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return photos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PhotoTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PhotoTableViewCell
        let photo = photos[indexPath.row]
        
        // Configure the cell...
        cell.parent = self
        cell.photo = photo
        cell.auth = self.auth
        cell.group = self.group
        cell.configure()
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat
        
        let width = UIScreen.main.bounds.size.width
        let photo = photos[indexPath.row]
        let orientation = photo.orientation
        let aspect = CGFloat(photo.aspect)
        
        if orientation == Orientation.UP || orientation == Orientation.DOWN {
            // Aspect ratio is correct
            height = width*aspect
        } else {
            // Inverse!
            height = width*(1/aspect)
        }
        
        return height + 94 // Very dirty. Feels hacky.
    }

    
    func updateGroupName(name: String) {
        Alamofire.request(Router.updateGroupName(auth: auth!, name: name, group: group!)!).responseJSON { response in
            let status = response.response?.statusCode
            if status == 200 {
                let json = JSON(response.result.value!)
                if json["Success"].exists() {
                    // Group title succesfully changed!
                    self.group?.title = name
                    NotificationCenter.default.post(name: NSNotification.Name("refresh-title"), object: nil)
                } else {
                    // Oops, probably server error. Could not change name.
                    // Should send a message to user about it.
                }
            }
        }
    }
    
    
    @IBAction func changeGroupName(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Change the Group Name",
                                                message: "",
                                                preferredStyle: UIAlertControllerStyle.alert
        )
        
        let saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.default,
            handler: { alert -> Void in
            
            let groupName = alertController.textFields![0] as UITextField
                
            if groupName.text != nil && groupName.text!.characters.count > 0 {
                self.updateGroupName(name: groupName.text!)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Let's call it"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    // MARK: - Navigation
    @IBAction func close(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowDetails" {
            let cell = sender as! PhotoTableViewCell
            let indexPath = tableView.indexPath(for: cell)!
            let selectedPhoto = photos[indexPath.row]
            let fullScreen = segue.destination as! PhotoFullViewController
            fullScreen.photo = selectedPhoto
            fullScreen.img_width = cell.img_photo.image?.size.width
            fullScreen.img_height = cell.img_photo.image?.size.height
            
        } else if segue.identifier == "ReloadBackground" {
            let backgroundController = segue.destination as! BackgroundViewController
            backgroundController.auth = self.auth
        
        } else if segue.identifier == "ShowMessages" {
            let indexPath : IndexPath
            if let button = sender as? UIButton {
                let cell = button.superview?.superview as! UITableViewCell
                indexPath = self.tableView.indexPath(for: cell)!
                let selectedPhoto = photos[indexPath.row]
                let detailController = segue.destination as! DetailedPhotoViewController
                detailController.auth = self.auth
                detailController.photo = selectedPhoto
                detailController.group = self.group
            }
        }
    }
    
}
