//
//  SignUpViewController.swift
//  partypic
//
//  Created by João Henrique Carneiro on 1/6/17.
//  Copyright © 2017 João Henrique Carneiro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SignUpViewController: _KeyboardViewController {

    // MARK: Properties
    var handler: FormHandler?
    
    @IBOutlet var form: [UITextField]!
    
    
    @IBOutlet weak var in_username: UITextField!
    @IBOutlet weak var in_realname: UITextField!
    @IBOutlet weak var in_email: UITextField!
    @IBOutlet weak var in_password: UITextField!
    @IBOutlet weak var in_confirm: UITextField!
    @IBOutlet weak var lb_message: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        in_username.addTarget(self, action: #selector(SignUpViewController.checkUsername(_:)), for: UIControlEvents.editingDidEnd)
        handler = FormHandler(inputs: form, completion: autoProcessForm)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        handler?.handle(textfield: textfield)
        return true
    }
    
    func checkUsername(_ textfield: UITextField) {
        if textfield.text!.characters.count > 0 {
            Alamofire.request(Router.checkUserExists(textfield.text!)!).responseJSON { response in
                let status : Int = (response.response?.statusCode)!
                if status == 200 {
                    let username = JSON(response.result.value!)
                    if username["Exists"].exists() {
                        self.lb_message.text = "Username already exists. Please use something else."
                        self.lb_message.isHidden = false
                    } else {
                        self.lb_message.isHidden = true
                    }
                }
            }
        }
    }
    
    func autoProcessForm() {
        processForm(UIButton())
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func processForm(_ sender: UIButton) {
        lb_message.isHidden = true
        if !in_username.text!.isEmpty && !in_email.text!.isEmpty && !in_password.text!.isEmpty {
            let username = self.in_username.text!
            let password = self.in_password.text!
            let email = self.in_email.text!
            let real_name = self.in_realname.text ?? ""
            
            Alamofire.request(Router.createUser(username, real_name: real_name, password: password, email: email)!).responseJSON { response in
                let status : Int = response.response!.statusCode
                if status == 200 {
                    let json = JSON(response.result.value!)
                    if json["Profile"].exists() { // Profile is only returned by server, if it was created.
                        self.performSegue(withIdentifier: "LoginRedirect", sender: self)
                    } else {
                        self.lb_message.text = "Could not create user account, at this moment. Try again later!"
                        self.lb_message.isHidden = false
                        
                    }
                }
            }
        }
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    

}
